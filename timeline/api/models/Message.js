/**
 * Message.js
 *
 * @description :: This represents a message posted by a user
 * @docs        :: http://sailsjs.org/documentation/concepts/models-and-orm/models
 */

module.exports = {
  attributes: {
    userName: {
      type: 'string',
      required: true
    },
    body: {
      type: 'string',
      required: true
    },
    comments: {
      collection: 'comment',
      via: 'messageId'
    }
  }
};

