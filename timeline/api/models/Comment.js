/**
 * Comment.js
 *
 * @description :: This represents a comment posted by user which is associated with a message
 * @docs        :: http://sailsjs.org/documentation/concepts/models-and-orm/models
 */

module.exports = {
  attributes: {
    userName: {
      type: 'string',
      required: true
    },
    body: {
      type: 'string',
      required: true
    },
    messageId: {
      model: 'message'
    }
  }
};

