angular.module('timelineApp').filter('moment', function () {
  /**
   * Custom filter that uses momentjs library to human format time
   */
  return function (input) {
    return moment(input).fromNow();
  }
});
