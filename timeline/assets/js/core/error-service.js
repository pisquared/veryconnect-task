angular.module('timelineApp').factory('ErrorService', function (NotifyingService) {
  /**
   * Flash error messages service
   */
  return {
    flash: function(message) {
      NotifyingService.notify(NEW_ERROR_EVENT, message);
    },
    unFlash: function() {
      NotifyingService.notify(NEW_ERROR_EVENT, "");
    }
  };
});
