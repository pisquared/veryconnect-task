var NEW_MESSAGE_EVENT = "new_message";
var NEW_COMMENT_EVENT = "new_comment";
var NEW_ERROR_EVENT = "new_error";

angular.module('timelineApp').factory('NotifyingService', function ($rootScope) {
  /**
   * Simple pub/sub service allowing to subscribe and notify on events
   */
  return {
    subscribe: function (messageName, scope, callback) {
      var handler = $rootScope.$on(messageName, callback);
      scope.$on('$destroy', handler);
    },

    notify: function (messageName, args) {
      $rootScope.$emit(messageName, args);
    }
  };
});
