angular.module('timelineApp').component('flash', {
  /**
   * Represents the flash/error bar element
   */
  templateUrl: 'templates/core/flash.template.html',
  controller: function FlashController($scope, NotifyingService) {
    // subscribe to receiving new error messages
    NotifyingService.subscribe(NEW_ERROR_EVENT, $scope, function newMessage(_, message) {
      $scope.flashMessage = message;
    });
  }
});
