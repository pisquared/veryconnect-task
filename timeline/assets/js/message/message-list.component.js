angular.module('timelineApp').component('messageList', {
  /**
   * Represents List of messages from users
   */
  templateUrl: 'templates/message/message-list.template.html',
  controller: function MessageListController($scope, $http, NotifyingService) {
    // subscribe to receiving new messages
    NotifyingService.subscribe(NEW_MESSAGE_EVENT, $scope, function newMessage(_, message) {
      $scope.messages.unshift(message);
    });
    // kick the process by getting the messages from the REST API
    $http.get('message').then(function (response) {
      $scope.messages = response.data;
    });
  }
});
