angular.module('timelineApp').component('messageForm', {
  /**
   * Represents the message form component allowing the user to create a new message
   */
  templateUrl: 'templates/message/message-form.template.html',
  controller: function MessageFormController($scope, $http, NotifyingService, ErrorService) {
    this.doesPresubmitValidates = function () {
      /**
       * Does pre-submit validation of the parameters.
       */
      if (!$scope.body || !$scope.userName) {
        ErrorService.flash("Enter both a message and a user name.");
        return false;
      }
      return true;
    };

    this.serverUpdate = function () {
      /**
       * Does the update of message on the server side
       */
      $http.post('message', {
          body: $scope.body,
          userName: $scope.userName
        },
        {headers: {'Content-Type': 'application/json'}}
      ).success(function (response) {
        NotifyingService.notify(NEW_MESSAGE_EVENT, response);
      }).error(function (error) {
        ErrorService.flash(error.message);
      });
    };

    this.addMessage = function () {
      /**
       * Notifies about adding a message, if validations pass, and updates server side.
       */
      ErrorService.unFlash();

      if (!this.doesPresubmitValidates())
        return;

      this.serverUpdate();

      // re-init scope
      $scope.body = "";
      $scope.userName = "";
    }
  }
});
