angular.module('timelineApp').component('commentList', {
  /**
   * Represents List of comments from users
   */
  bindings: {
    'messageId': '<',
    'comments': '<'
  },
  templateUrl: 'templates/comment/comment-list.template.html',
  controller: function CommentListController($scope, $http, NotifyingService) {
    $scope.comments = this.comments;
    $scope.messageId = this.messageId;
    // subscribe to receiving new comments
    NotifyingService.subscribe(NEW_COMMENT_EVENT, $scope, function newComment(_, comment) {
      if (comment.messageId === $scope.messageId)
        $scope.comments.unshift(comment);
    });
  }
});
