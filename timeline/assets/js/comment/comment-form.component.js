angular.module('timelineApp').component('commentForm', {
  /**
   * Represents the comment form component allowing the user to create a new message
   */
  bindings: {
    'messageId': '<'
  },
  templateUrl: 'templates/comment/comment-form.template.html',
  controller: function CommentFormController($scope, $http, NotifyingService, ErrorService) {
    $scope.messageId = this.messageId;

    this.doesPresubmitValidates = function () {
      /**
       * Does pre-submit validation of the parameters.
       */
      if (!$scope.body || !$scope.userName) {
        ErrorService.flash("Enter both a message and a user name.");
        return false;
      }
      return true;
    };

    this.serverUpdate = function () {
      /**
       * Does the update of message on the server side
       */
      $http.post('comment', {
          messageId: $scope.messageId,
          body: $scope.body,
          userName: $scope.userName
        },
        {headers: {'Content-Type': 'application/json'}}
      ).success(function (response) {
        NotifyingService.notify(NEW_COMMENT_EVENT, response);
      }).error(function (error) {
        ErrorService.flash(error.message);
      });
    };

    this.addComment = function () {
      /**
       * Adds a comment to a message
       */
      ErrorService.unFlash();

      if (!this.doesPresubmitValidates())
        return;

      $scope.isCommentFormOpen = false;
      this.serverUpdate();

      // re-init scope
      $scope.body = "";
      $scope.userName = "";
    };
  }
});
