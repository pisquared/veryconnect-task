# To setup
Install sails globally
```
npm install sails sails-mongo -g
```

Install Mongodb:
```
sudo apt install mongodb
```

Start the server:

```
cd timeline
sails lift
```

# Description
The task was done following boilerplate init of a sails app, 
using the framework to create the `Message` and `Comment` APIs.

The frontend is done using Angular 1.5.6 and the main front end code is in
`assets/js/core|comment|message`.

# Timelog

## 02-08-2017: 20:00 - 21:00
* Setup dev env with node
* Setup initial Sails app

## 03-08-2017: 18:00 - 19:00
* Bootstrap initial design of the page
* Generated Message, Comment API models
* Included Angular with simple static component of messages

## 04-08-2017 20:00 - 21:00
* Install mongo
* Connect sails with mongo
* Angular get data with xhr call from sails

## 06-08-2017 15:00 - 18:00
* POST a message xhr and communicate between components
* Nested components in angular
* autopopulate comments in sails
* Hide comment form

## 07-08-2017 08:00 - 09:00
* Form validation with error service

## 08-08-2017 19:00 - 19:30
* Refactoring